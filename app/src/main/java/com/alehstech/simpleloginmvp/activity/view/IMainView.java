package com.alehstech.simpleloginmvp.activity.view;

import com.alehstech.simpleloginmvp.activity.model.Companies;

import java.util.List;

public interface IMainView {
    void onImageResponse(List<Companies.Department> companies);
    void onImageResponseError(String errorMessage);
}
