package com.alehstech.simpleloginmvp.activity.presenter;

public interface ILoginPresenter {
    public void clear();
    //public void doLogin(String userName,String password);
    void onLoginResponse(boolean isLogin);
    void onLoginError(String  msg);
}
