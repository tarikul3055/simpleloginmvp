package com.alehstech.simpleloginmvp.activity.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alehstech.simpleloginmvp.R;
import com.alehstech.simpleloginmvp.activity.presenter.LoginPresenterCompl;
import com.alehstech.simpleloginmvp.utils.SpinDialog;

public class LoginActivity extends AppCompatActivity implements  ILoginView {
    private EditText editUser;
    private EditText editPass;
    private Button btnLogin;
    private Button   btnClear;
    private TextView tvSuccessResult;

    private LoginPresenterCompl loginPresenter;
    SpinDialog spinDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
    }

    public void initViews(){
        //find view
        editUser = (EditText) this.findViewById(R.id.et_login_username);
        editPass = (EditText) this.findViewById(R.id.et_login_password);
        btnLogin = (Button) this.findViewById(R.id.btn_login_login);
        btnClear = (Button) this.findViewById(R.id.btn_login_clear);
        tvSuccessResult = findViewById(R.id.tv_success_result);

        //init
        loginPresenter = new LoginPresenterCompl(this);



        //set listener
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   loginPresenter.setProgressBarVisiblity(View.GONE);

//                btnLogin.setEnabled(false);
//                btnClear.setEnabled(false);
                String userName = editUser.getText().toString();
                String password = editPass.getText().toString();
                if(userName.trim().equals("")){
                    tvSuccessResult.setText("Please give user name !");
                    tvSuccessResult.setTextColor(Color.RED);
                    return;
                }
                if(password.trim().equals("")){
                    tvSuccessResult.setText("Please give user password !");
                    tvSuccessResult.setTextColor(Color.RED);
                    return;
                }
                if(userName.trim().length()>0){
                    spinDialog = new SpinDialog(LoginActivity.this, false);
                    spinDialog.show();

                    loginPresenter.doLogin(userName,password);
                }



            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPresenter.clear();
            }
        });


    }



    @Override
    public void clearText() {
            editUser.setText("");
            editPass.setText("");
    }

    @Override
    public void onLoginResponse(boolean isLogin) {
              if (isLogin){
                  spinDialog.dismis();
                  Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                  startActivity(intent);
              }
    }

    @Override
    public void onLoginError(String msg) {
        if(msg!=null){
            spinDialog.dismis();
            tvSuccessResult.setText(msg);
        }

    }





    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}