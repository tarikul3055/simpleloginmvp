package com.alehstech.simpleloginmvp.activity.model;

import com.alehstech.simpleloginmvp.activity.model.Companies;
import com.alehstech.simpleloginmvp.activity.presenter.IMainActPresenter;
import com.alehstech.simpleloginmvp.network.ApiService;
import com.alehstech.simpleloginmvp.network.NetworkCall;
import com.alehstech.simpleloginmvp.network.ResponseCallback;
import com.alehstech.simpleloginmvp.activity.presenter.ILoginPresenter;


import java.util.List;

public class MainActModel {


    public static void getAllDeptName(IMainActPresenter iMainActPresenter){
        ApiService apiService = new NetworkCall();
        apiService.getAllDepartmentList(new ResponseCallback<List<Companies.Department>>() {
            @Override
            public void onSuccess(List<Companies.Department> data) {
                iMainActPresenter.onImageResponse(data);
            }

            @Override
            public void onError(String errorMessage) {
                iMainActPresenter.onImageResponseError(errorMessage);
            }
        });
    }

    public static void doLogin(String userName, String password, ILoginPresenter iMainActPresenter){
        ApiService apiService = new NetworkCall();
        apiService.doLogin(new ResponseCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean data) {
                iMainActPresenter.onLoginResponse(data);
            }

            @Override
            public void onError(String errorMessage) {
                iMainActPresenter.onLoginError(errorMessage);
            }
        }, userName, password);

    }
}
