package com.alehstech.simpleloginmvp.activity.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.alehstech.simpleloginmvp.R;
import com.alehstech.simpleloginmvp.activity.presenter.MainPresenterCompl;
import com.alehstech.simpleloginmvp.activity.adapters.DepartmentAdapter;
import com.alehstech.simpleloginmvp.activity.model.Companies;

import com.alehstech.simpleloginmvp.utils.SpinDialog;

import java.util.List;

public class MainActivity extends AppCompatActivity implements IMainView {
    private RecyclerView rcvDept;
    MainPresenterCompl mainPresenterCompl;
    SpinDialog spinDialog;
    LinearLayoutManager linearLayoutManager;
    DepartmentAdapter deptAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }
    public void initViews(){
        rcvDept = findViewById(R.id.rcv_dept);
        mainPresenterCompl = new MainPresenterCompl(this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcvDept.setLayoutManager(linearLayoutManager);
        rcvDept.setItemAnimator(new DefaultItemAnimator());
        spinDialog = new SpinDialog(MainActivity.this, false);
        spinDialog.show();
        mainPresenterCompl.callForDepytList();

    }

    @Override
    public void onImageResponse(List<Companies.Department> departmentList) {
        spinDialog.dismis();
        deptAdapter = new DepartmentAdapter(MainActivity.this, departmentList);
        rcvDept.setAdapter(deptAdapter);
    }

    @Override
    public void onImageResponseError(String errorMessage) {
        Log.i("onImageLoadError", "onImageResponseError: " + errorMessage);
    }
}