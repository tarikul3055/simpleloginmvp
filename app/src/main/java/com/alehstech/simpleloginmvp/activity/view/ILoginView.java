package com.alehstech.simpleloginmvp.activity.view;

public interface ILoginView {
    public void clearText();
    void onLoginResponse(boolean isLogin);
    void onLoginError(String  msg);
}
