package com.alehstech.simpleloginmvp.activity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.alehstech.simpleloginmvp.R;
import com.alehstech.simpleloginmvp.activity.model.Companies;

import java.util.ArrayList;
import java.util.List;

//import com.djd.allthananumber.R;


public class DepartmentAdapter extends
        RecyclerView.Adapter<DepartmentAdapter.DeptViewHolder> {

    List<Companies.Department> departmentList = new ArrayList<>();
    Context mContext;

    protected LayoutInflater vi;

    String mechanicId = " ";
    private OnItemClickListener mListener;
    private boolean isLoadingAdded = false;


    public interface OnItemClickListener {
        void onMenuClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public DepartmentAdapter(Context context,List<Companies.Department> departmentList) {
        mContext = context;
        this.departmentList = departmentList;

    }

    @NonNull
    @Override
    public DepartmentAdapter.DeptViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_list, parent, false);
        return new DeptViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final DepartmentAdapter.DeptViewHolder viewHolder,
                                 final int position) {
        Companies.Department item = departmentList.get(position);

        String name = item.getName();

        if(name!=null){
            viewHolder.tvDeptName.setText(name);
        }



    }

    @Override
    public int getItemCount() {
        return departmentList.size();
    }

    public class DeptViewHolder extends RecyclerView.ViewHolder {
        TextView tvDeptName;


        private DeptViewHolder(View itemView) {
            super(itemView);
            tvDeptName = (TextView) itemView.findViewById(R.id.tv_dept_name);

        }
    }



}
