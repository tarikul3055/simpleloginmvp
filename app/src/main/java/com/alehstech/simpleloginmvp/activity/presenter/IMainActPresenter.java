package com.alehstech.simpleloginmvp.activity.presenter;



import com.alehstech.simpleloginmvp.activity.model.Companies;

import java.util.List;

public interface IMainActPresenter {
    void onImageResponse(List<Companies.Department> companies);
    void onImageResponseError(String errorMessage);
}
