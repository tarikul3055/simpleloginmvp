package com.alehstech.simpleloginmvp.activity.presenter;

import com.alehstech.simpleloginmvp.activity.model.MainActModel;
import com.alehstech.simpleloginmvp.activity.view.ILoginView;

public class LoginPresenterCompl implements ILoginPresenter {
    ILoginView iLoginView;



    public LoginPresenterCompl(ILoginView iLoginView) {
        this.iLoginView = iLoginView;

    }




    @Override
    public void clear() {
           iLoginView.clearText();
    }

    @Override
    public void onLoginResponse(boolean isLogin) {
        iLoginView.onLoginResponse(isLogin);
    }

    @Override
    public void onLoginError(String msg) {
        iLoginView.onLoginError(msg);
    }


    public void doLogin(String userName, String password) {

            MainActModel.doLogin(userName,password,this);

    }


}
