package com.alehstech.simpleloginmvp.activity.presenter;



import com.alehstech.simpleloginmvp.activity.model.Companies;
import com.alehstech.simpleloginmvp.activity.model.MainActModel;
import com.alehstech.simpleloginmvp.activity.view.IMainView;

import java.util.List;

public class MainPresenterCompl implements IMainActPresenter{
    IMainView iMainView;

    public MainPresenterCompl(IMainView iMainView) {
        this.iMainView = iMainView;
    }

    public void callForDepytList() {
        MainActModel.getAllDeptName(this);
    }


    @Override
    public void onImageResponse(List<Companies.Department> imageModelList) {
          iMainView.onImageResponse(imageModelList);
    }

    @Override
    public void onImageResponseError(String errorMessage) {
        iMainView.onImageResponseError(errorMessage);
    }
}
