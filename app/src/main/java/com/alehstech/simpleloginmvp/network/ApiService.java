package com.alehstech.simpleloginmvp.network;


import com.alehstech.simpleloginmvp.activity.model.Companies;

import java.util.List;

public interface ApiService {

    void getAllDepartmentList(ResponseCallback<List<Companies.Department>> responseCallback);
    void doLogin(ResponseCallback<Boolean> responseCallback,String userName,String password);

}
