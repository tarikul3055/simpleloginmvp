package com.alehstech.simpleloginmvp.network;



import com.alehstech.simpleloginmvp.activity.model.Companies;
import com.alehstech.simpleloginmvp.activity.model.LoginData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkCall implements ApiService{


    @Override
    public void getAllDepartmentList(ResponseCallback<List<Companies.Department>> responseCallback) {
        RetAPIService apiInterface = RetroClient.getClient().create(RetAPIService.class);
        Call<Companies> call = apiInterface.getAllDept();
        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                if (response.code() == 200){
                    responseCallback.onSuccess(response.body().getDepartments());
                }else {
                    responseCallback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                responseCallback.onError(t.getMessage());
            }
        });
    }

    @Override
    public void doLogin(ResponseCallback<Boolean> responseCallback, String userName, String password) {
        RetAPIService apiInterface = RetroClient.getClient().create(RetAPIService.class);
        Call<LoginData> call = apiInterface.tryToPostLoginData(userName,password);
        call.enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if (response.code() == 200){
                    responseCallback.onSuccess(true);
                }else {
                    responseCallback.onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                responseCallback.onError(t.getMessage());
            }
        });

    }


}
