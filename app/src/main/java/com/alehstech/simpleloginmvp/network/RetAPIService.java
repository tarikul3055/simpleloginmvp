package com.alehstech.simpleloginmvp.network;

import com.alehstech.simpleloginmvp.activity.model.Companies;
import com.alehstech.simpleloginmvp.activity.model.LoginData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetAPIService {
    @FormUrlEncoded
    @POST("user-login")
    Call<LoginData> tryToPostLoginData(@Field("email") String email, @Field("password") String password);

    @GET("get-companies")
    Call<Companies> getAllDept();
}
